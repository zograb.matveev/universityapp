package am.matveev.spring.University.controllers;

import am.matveev.spring.University.dto.CourseDTO;
import am.matveev.spring.University.dto.StudentDTO;
import am.matveev.spring.University.entities.StudentEntity;
import am.matveev.spring.University.services.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/student")
@RestController
@RequiredArgsConstructor
public class StudentController{

    private final StudentService studentService;

    @GetMapping()
    public List<StudentDTO> findAll(){
        return studentService.findAll();
    }

    @GetMapping("/{id}")
    public StudentDTO findOne(@PathVariable long id){
        return studentService.findOne(id);
    }

    @PostMapping()
    public StudentDTO create(@RequestBody @Valid StudentDTO studentDTO){
        return studentService.create(studentDTO);
    }

    @PutMapping("/{id}")
    public StudentDTO update(@PathVariable long id,@RequestBody @Valid StudentDTO studentDTO){
        return studentService.update(id,studentDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        studentService.delete(id);
    }

    @PostMapping("/{studentId}/enroll")
    public ResponseEntity<CourseDTO> enrollCourseWithStudent(@PathVariable long studentId,
                                                             @RequestParam long courseId){
        studentService.enrollCourseWithStudent(studentId,courseId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
