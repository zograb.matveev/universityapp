package am.matveev.spring.University.controllers;

import am.matveev.spring.University.dto.CourseDTO;
import am.matveev.spring.University.services.CourseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/course")
@RestController
@RequiredArgsConstructor
public class CourseController{

    private final CourseService courseService;

    @GetMapping()
    public List<CourseDTO> findAll(){
        return courseService.findAll();
    }

    @GetMapping("/{id}")
    public CourseDTO findOne(@PathVariable long id){
        return courseService.findOne(id);
    }

    @PostMapping()
    public CourseDTO create(@RequestBody @Valid CourseDTO courseDTO){
        return courseService.create(courseDTO);
    }

    @PutMapping("/{id}")
    public CourseDTO update(@PathVariable long id,@RequestBody @Valid CourseDTO courseDTO){
        return courseService.update(id,courseDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        courseService.delete(id);
    }
}
