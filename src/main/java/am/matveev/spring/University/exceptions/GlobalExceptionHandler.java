package am.matveev.spring.University.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler({StudentNotFoundException.class})
    public ResponseEntity handleException(StudentNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Student with this id wasn't found!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({CourseNotFoundException.class})
    public ResponseEntity handleException(CourseNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Course with this id wasn't found!",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }
}
