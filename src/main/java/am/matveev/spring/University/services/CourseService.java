package am.matveev.spring.University.services;

import am.matveev.spring.University.dto.CourseDTO;
import am.matveev.spring.University.entities.CourseEntity;
import am.matveev.spring.University.exceptions.CourseNotFoundException;
import am.matveev.spring.University.mappers.CourseMapper;
import am.matveev.spring.University.repositories.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CourseService{

    private final CourseRepository courseRepository;
    private final CourseMapper courseMapper;

    @Transactional(readOnly = true)
    public List<CourseDTO> findAll(){
        List<CourseEntity> course = courseRepository.findAll();
        List<CourseDTO> courseDTOS = course.stream()
                .map(courseMapper::toDTO)
                .collect(Collectors.toList());
        return courseMapper.toDTOsWithStudents(course);
    }

    @Transactional(readOnly = true)
    public CourseDTO findOne(long id){
        CourseEntity course = courseRepository.findById(id)
                .orElseThrow(CourseNotFoundException ::new);
        return courseMapper.toDTO(course);
    }

    @Transactional
    public CourseDTO create(CourseDTO courseDTO){
        CourseEntity course = courseMapper.toEntity(courseDTO);
        courseRepository.save(course);
        return courseDTO;
    }

    @Transactional
    public CourseDTO update(long id,CourseDTO updatedCourse){
        CourseEntity course = courseMapper.toEntity(updatedCourse);
        course.setId(id);
        courseRepository.save(course);
        return courseMapper.toDTO(course);
    }

    @Transactional
    public void delete(long id){
        courseRepository.deleteById(id);
    }
}
