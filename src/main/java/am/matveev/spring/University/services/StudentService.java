package am.matveev.spring.University.services;

import am.matveev.spring.University.dto.CourseDTO;
import am.matveev.spring.University.dto.StudentDTO;
import am.matveev.spring.University.entities.CourseEntity;
import am.matveev.spring.University.entities.StudentEntity;
import am.matveev.spring.University.exceptions.CourseNotFoundException;
import am.matveev.spring.University.exceptions.StudentNotFoundException;
import am.matveev.spring.University.mappers.StudentMapper;
import am.matveev.spring.University.repositories.CourseRepository;
import am.matveev.spring.University.repositories.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class StudentService{

    private final StudentMapper studentMapper;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;

    @Transactional(readOnly = true)
    public List<StudentDTO> findAll(){
       List<StudentEntity> students = studentRepository.findAll();
        List<StudentDTO> studentDTOS = students.stream()
                .map(studentMapper::toDTO)
                .collect(Collectors.toList());
        return studentDTOS;
    }

    @Transactional(readOnly = true)
    public StudentDTO findOne(long id){
        StudentEntity student = studentRepository.findById(id)
                .orElseThrow(StudentNotFoundException ::new);
        return studentMapper.toDTO(student);
    }

    @Transactional
    public StudentDTO create(StudentDTO studentDTO){
        StudentEntity studentEntity = studentMapper.toEntity(studentDTO);
        studentRepository.save(studentEntity);
        return studentDTO;
    }

    @Transactional
    public StudentDTO update(long id,StudentDTO studentDTO){
        StudentEntity student = studentMapper.toEntity(studentDTO);
        student.setId(id);
        studentRepository.save(student);
        return studentDTO;
    }

    @Transactional
    public void delete(long id){
        studentRepository.deleteById(id);
    }

    @Transactional
    public ResponseEntity<CourseDTO> enrollCourseWithStudent(long studentId, long courseId){
        CourseEntity course = courseRepository.findById(courseId)
                .orElseThrow(CourseNotFoundException ::new);

        StudentEntity student = studentRepository.findById(studentId)
                .orElseThrow(StudentNotFoundException ::new);

        student.setCourse(course);
        studentRepository.save(student);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
