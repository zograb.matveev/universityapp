package am.matveev.spring.University.mappers;

import am.matveev.spring.University.dto.CourseDTO;
import am.matveev.spring.University.entities.CourseEntity;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourseMapper{

    CourseDTO toDTO(CourseEntity course);
    CourseEntity toEntity(CourseDTO courseDTO);

    @Mapping(target = "studentDTOS", source = "students")
    @Named("toDTOWithStudents")
    CourseDTO toDTOWithStudents(CourseEntity courseEntity);

    @IterableMapping(qualifiedByName = "toDTOWithStudents")
    List<CourseDTO> toDTOsWithStudents(List<CourseEntity> courseEntities);
}
