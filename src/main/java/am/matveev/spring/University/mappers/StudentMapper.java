package am.matveev.spring.University.mappers;

import am.matveev.spring.University.dto.StudentDTO;
import am.matveev.spring.University.entities.StudentEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StudentMapper{

    StudentDTO toDTO(StudentEntity student);
    StudentEntity toEntity(StudentDTO studentDTO);
}
