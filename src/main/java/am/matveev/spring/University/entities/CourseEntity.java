package am.matveev.spring.University.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "Course")
@Getter
@Setter
public class CourseEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "course")
    private List<StudentEntity> students;
}
