package am.matveev.spring.University.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "Student")
@Getter
@Setter
public class StudentEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "age")
    private int age;

    @Column(name = "date_of_admission")
    private int dateOfAdmission;

    @ManyToOne()
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private CourseEntity course;
}
