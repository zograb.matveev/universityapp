package am.matveev.spring.University.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CourseDTO{

    @NotEmpty(message = "Course name should not be empty")
    private String courseName;

    @NotEmpty(message = "Description should not be empty")
    private String description;

    private List<StudentDTO> studentDTOS;
}
